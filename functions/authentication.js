const admin = require('firebase-admin')

const getIdToken = (request, hasAuthorizationHeader) => hasAuthorizationHeader
  ? request.headers.authorization.split('Bearer ')[1]
  : request.cookies.__session

const firebaseIdentify = (request, response, next) => {
  const hasAuthorizationHeader = request.headers.authorization &&
    !request.headers.authorization.startsWith('Bearer ')

  if (!hasAuthorizationHeader && !request.cookies.__session) {
    console.warn('No Firebase ID token was included in the request.')
    request.user = false

    return next()
  }

  const idToken = getIdToken(request, hasAuthorizationHeader)

  return admin.auth().verifyIdToken(idToken).then(user => {
    request.user = user
    request.isMe = otherUser => otherUser.uid === user.uid

    return next()
  }).catch(error => {
    console.warn('Error while verifying Firebase ID token:', error)
    request.user = false

    return next()
  })
}

const firebaseAuthenticate = (request, response, next) => request.user
  ? next()
  : response.status(401).render(
    'error',
    {
      message: 'You need to log in to access this page',
      quote: 'That is certainly one way of entering without anyone seeing your face',
      number: 401
    }
  )

module.exports = {
  firebaseIdentify,
  firebaseAuthenticate
}
