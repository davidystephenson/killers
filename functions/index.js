const admin = require('firebase-admin')
const functions = require('firebase-functions')
const express = require('express')
const cookieParser = require('cookie-parser')
const cors = require('cors')
const router = require('./router')
const jsmile = require('jsmile')

// TODO split into files

const ALPHABET = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'
  .split('')

const shuffle = array => array
  .map((a) => [Math.random(), a])
  .sort((a, b) => a[0] - b[0])
  .map((a) => a[1])

// Log promise errors
process.on(
  'unhandledRejection',
  (reason, p) => {
    console.error('Unhandled Rejection at: Promise', p, 'reason:', reason, 'stack:', reason.stack)
  }
)

const serviceAccount = require('./key.json')
admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
  databaseURL: 'https://killers-812ba.firebaseio.com'
})
const db = admin.firestore()

const app = express()
app.use(cors({ origin: true }))
app.use(cookieParser())
app.use('/', router(db))

const gamesRef = db.collection('games')

app.set('views', './views')
app.set('view engine', 'js')
app.engine('js', jsmile.express(app))

exports.app = functions.https.onRequest(app)

exports.onConnect = functions
  .https
  .onCall((data, context) => 'You are connected.')

exports.newGame = functions
  .https
  .onCall((data, context) => {
    if (context.auth.uid) {
      return gamesRef
        .add({
          creatorId: context.auth.uid,
          playersCount: 0,
          alphabet: shuffle(ALPHABET)
        })
        .then(gameRef => ({ gameId: gameRef.id }))
    } else {
      throw new functions.https.HttpsError(
        'permission-denied',
        'You need to register or login to create a game.'
      )
    }
  })

exports.joinGame = functions
  .https
  .onCall((data, context) => {
    if (context.auth.uid) {
      const gameRef = gamesRef.doc(data.gameId)

      return gameRef
        .get()
        .then(doc => {
          if (doc.exists) {
            const game = doc.data()
            if (game.alphabet.length) {
              const letter = game.alphabet.pop()

              const playersRef = gameRef.collection('players')

              const playerRef = playersRef.doc(letter)

              const batch = db.batch()
              batch
                .set(
                  playerRef,
                  { userId: context.auth.uid }
                )
              batch
                .update(
                  gameRef,
                  { alphabet: game.alphabet }
                )

              return batch
                .commit()
                .then(() => {
                  const playerData = { playerId: letter }
                  return playerData
                })
            } else {
              throw new functions.https.HttpsError(
                'resource-exhausted',
                'The game has the maximum number of players.'
              )
            }
          } else {
            throw new functions.https.HttpsError(
              'not-found',
              'The game does not exist.'
            )
          }
        })
    } else {
      throw new functions.https.HttpsError(
        'permission-denied',
        'You need to register or login to join a game.'
      )
    }
  })
