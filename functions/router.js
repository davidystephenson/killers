const express = require('express')
const authentication = require('./authentication')
const admin = require('firebase-admin')

module.exports = db => {
  const gamesRef = db.collection('games')

  const router = express.Router()

  const _throw = error => { throw error }

  router.use(authentication.firebaseIdentify)

  router.get('/', (request, response) => {
    admin
      .auth()
      .listUsers()
      .then(result => response
        .render('index', { request, result })
      ).catch(_throw)
  })

  router.get(
    '/users?/:uid',
    authentication.firebaseAuthenticate,
    (request, response) => {
      const userNotFound = () => response
        .status(404)
        .render('error', {
          message: 'No user with that ID exists. Is your link valid?',
          quote: 'Look around you, and all you see are people the world would be better without',
          // TODO rename status
          number: 404
        })

      return admin.auth().getUser(request.params.uid)
        .then(user => user
          ? response.render('user', { request, user })
          : userNotFound()
        ).catch(error => error.message === 'There is no user record corresponding to the provided identifier.'
          ? userNotFound()
          : _throw(error)
        )
    }
  )

  router.get(
    '/games?/:id',
    authentication.firebaseAuthenticate,
    (request, response) => {
      const gameRef = gamesRef.doc(request.params.id)
      gameRef.get()
        .then(doc => response
          .render('game', { request, game: doc })
        )
    }
  )

  router.get('/test', (request, response) => {
    response.send('hi')
  })

  return router
}
