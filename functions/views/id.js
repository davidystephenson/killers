module.exports = (jsmile, options) => [
  {
    tag: 'span',
    class: 'id',
    child: `${options.text}`
  },
  jsmile.depend('id')
]
