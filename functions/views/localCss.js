module.exports = () => [
  { tag: 'link', rel: 'stylesheet', href: '/reset.css' },
  { tag: 'link', rel: 'stylesheet', href: '/style.css' }
]
