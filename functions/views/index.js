module.exports = (jsmile, options) => {
  return jsmile.include(
    'page',
    {
      request: options.request,

      quote: 'We are all equal in the eyes of the reaper',

      child: [
        options.request.user
          ? jsmile.include('games')
          : {
            tag: 'p',
            class: 'rule',
            child: 'Sign in to play.'
          },

        { tag: 'h2', child: 'Users' },

        options.result.users.map(
          user => jsmile.include(
            'userLink',
            { user, request: options.request }
          )
        )
      ]
    }
  )
}
