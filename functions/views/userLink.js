const getName = user => user.name || user.displayName
const getEmail = (user, request) => request.user && request.user.uid === user.uid ? ` (${user.email})` : ''

module.exports = (jsmile, options) => {
  return {
    class: 'user-link',
    child: {
      tag: 'a',
      href: `/user/${options.user.uid}`,
      child: [
        getName(options.user),
        getEmail(options.user, options.request),
        jsmile.include(
          'tag',
          {
            text: jsmile.build(
              jsmile.include(
                'id',
                {
                  text: options.user.uid
                }
              )
            )
          }
        )
      ]
    }
  }
}
