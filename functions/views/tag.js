module.exports = (jsmile, options) => ({
  tag: 'span',
  class: 'tag',
  child: [
    `[${options.text}]`,
    jsmile.depend('tag')
  ]
})
