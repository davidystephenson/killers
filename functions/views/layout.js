module.exports = (jsmile, options) => jsmile.include(
  'html',
  {
    head: [
      {
        tag: 'title',
        child: options.title ? `Killers - ${options.title}` : 'Killers'
      },
      ...jsmile.include('meta'),
      ...options.head,
      ...jsmile.include('localCss')
    ],
    body: {
      class: 'container',
      child: options.child
    }
  }
)
