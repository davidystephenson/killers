module.exports = () => [
  { tag: 'meta', charset: 'UTF-8' },
  {
    tag: 'meta',
    name: 'viewport',
    content: 'width=device-width, initial-scale=1'
  }
]
