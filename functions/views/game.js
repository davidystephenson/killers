module.exports = (jsmile, options) => jsmile
  .include('page', {
    request: options.request,

    quote: "If you can't win the game, if you can't solve the puzzle...",

    title: [
      'Game',
      jsmile
        .include('id', { text: options.game.id })
    ],

    child: [
      options.request.user
        ? jsmile.include(
          'joinGame', { gameId: options.game.id }
        )
        : null,

      jsmile.include(
        'list', {
          title: 'players',
          collection: `games/${options.game.id}/players`
        }
      )
    ]
  })
