module.exports = (jsmile, options) => [
  { tag: 'button', child: 'Join', 'data-gameid': options.gameId, class: 'join-game' },
  jsmile.depend('joinGame')
]
