module.exports = (jsmile, options) => [
  {
    tag: 'button',
    class: 'new-game',
    child: 'New game'
  },
  jsmile.depend('newGame')
]
