module.exports = (jsmile, options) => {
  const section = jsmile.include('section', options)

  const container = section[1]
  container.class += ' list'
  container['data-collection'] = options.collection
    ? options.collection
    : options.title
  container['data-where'] = options.where

  jsmile.depend('list')

  return section
}
