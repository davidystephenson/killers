module.exports = (jsmile, options) => ({
  tag: 'html',
  child: [
    { tag: 'head', child: options.head },
    { tag: 'body', child: options.body }
  ]
})
