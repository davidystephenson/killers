module.exports = (jsmile, options) => jsmile.include('layout', {
  head: [],
  child: [
    {
      tag: 'h1',
      child: options.number
        ? `${options.message} (${options.number})`
        : options.message
    },
    jsmile.include('quote', { child: options.quote })
  ]
})
