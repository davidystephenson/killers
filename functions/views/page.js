module.exports = (jsmile, options) => {
  options.data = options.data || {}
  options.data.user = options.request.user

  const user = options.request.user && {
    class: 'user js-authorized',
    child: [
      jsmile.include('userLink', { user: options.request.user, request: options.request }),
      { tag: 'button', id: 'sign-out', class: 'js-authorized', child: 'Sign Out' }
    ]
  }

  const firebaseUi = {
    id: 'firebaseui-auth-container',
    class: 'js-unauthorized firebaseui-auth-container hide'
  }

  const account = {
    child: {
      class: 'account',
      child: [user, firebaseUi]
    }
  }

  const content = {
    class: 'content',
    child: [
      { tag: 'h1', child: options.title || 'Killers' },
      options.quote
        ? jsmile.include(
          'quote', { child: options.quote }
        )
        : null
    ].concat(options.child)
  }

  const serverData = {
    tag: 'script',
    child: `window.serverData = ${JSON.stringify(options.data)}`
  }

  const initializationJs = [
    '<!-- Intialization -->',
    { tag: 'script', src: 'https://www.gstatic.com/firebasejs/5.0.4/firebase-app.js' },
    { tag: 'script', src: 'https://www.gstatic.com/firebasejs/5.0.4/firebase-auth.js' },
    { tag: 'script', src: 'https://www.gstatic.com/firebasejs/5.0.4/firebase-firestore.js' },
    { tag: 'script', src: 'https://www.gstatic.com/firebasejs/5.0.4/firebase-functions.js' },
    { tag: 'script', src: '/init.js' }
  ]

  const authenticationJs = [
    '<!-- Authentication -->',
    { tag: 'script', src: '/firebaseui.js' },
    { tag: 'script', src: 'https://cdn.jsdelivr.net/npm/js-cookie@2/src/js.cookie.min.js' },
    { tag: 'script', src: '/auth.js' }
  ]

  const logic = [
    ...initializationJs,
    ...authenticationJs,
    '<!-- Logic -->',
    { tag: 'script', src: '/jsmile.js' },
    jsmile.dependency(
      'list',
      { tag: 'script', src: '/list.js' }
    ),
    jsmile.dependency(
      'newGame',
      { tag: 'script', src: '/newGame.js' }
    ),
    jsmile.dependency(
      'joinGame',
      { tag: 'script', src: '/joinGame.js' }
    ),
    jsmile.dependency(
      'tag',
      {
        tag: 'link',
        rel: 'stylesheet',
        type: 'text/css',
        href: '/tag.css'
      }
    ),
    jsmile.dependency(
      'id',
      {
        tag: 'link',
        rel: 'stylesheet',
        type: 'text/css',
        href: '/id.css'
      }
    )
  ]

  return jsmile.include(
    'layout',
    {
      head: [{
        tag: 'link',
        type: 'text/css',
        rel: 'stylesheet',
        href: 'https://cdn.firebase.com/libs/firebaseui/2.6.0/firebaseui.css'
      }],
      child: {
        class: 'layout',
        child: [account, content, serverData, ...logic]
      },
      title: options.title
    }
  )
}
