module.exports = (jsmile, options) => jsmile.include(
  'page',
  {
    request: options.request,

    title: {
      tag: 'a',
      href: `/users/${options.user.uid}`,
      child: [
        'User',
        jsmile
          .include('id', { text: options.user.uid })
      ]
    },

    child: options.request.isMe(options.user)
      ? [
        jsmile.include(
          'quote',
          { child: 'Being alone is better than being with the wrong person' }
        ),
        jsmile.include('userLink', options)
      ]
      : jsmile.include(
        'quote',
        { child: 'In this world, there are very few people who actually trust each other' }
      )
  }
)
