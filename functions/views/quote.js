module.exports = (jsmile, options) => ({
  class: 'quote',
  child: { tag: 'span', child: options.child }
})
