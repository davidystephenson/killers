module.exports = (jsmile, options) => [
  { tag: 'h2', child: options.title },
  { class: options.title.split(' ').join('-') },
  jsmile.depend(options.title)
]
