// MDN polyfill
if (!Object.entries) {
  Object.entries = function (obj) {
    var ownProps = Object.keys(obj)
    var i = ownProps.length
    var resArray = new Array(i)
    while (i--) {
      resArray[i] = [ownProps[i], obj[ownProps[i]]]
    }

    return resArray
  }
}

const path = require('path')

const SELF_CLOSING_TAGS = [
  'area',
  'base',
  'br',
  'col',
  'command',
  'embed',
  'hr',
  'img',
  'input',
  'keygen',
  'link',
  'meta',
  'param',
  'source',
  'track',
  'wbr'
]
const isSelfClosing = tag => SELF_CLOSING_TAGS.indexOf(tag) > -1

const isString = value => typeof value === 'string'
const isArray = value => Array.isArray(value)
const isObject = value => !Array.isArray(value) && value === Object(value)
const isFunction = value => value && {}.toString.call(value) === '[object Function]'
const isNumber = value => !isNaN(value) && typeof value === 'number'

const isValid = value => isString(value) || isArray(value) || isObject(value) || isFunction(value) || isNumber(value)
const isOnlyObject = value => isObject(value) && !isFunction(value) && !isArray(value)

const buildTag = element => {
  element.tag = `<${element.data.tag}`

  // Attach the the data properties as HTML attributes.
  if (element.data.className) {
    element.data.class = element.data.className
  }

  const filter = property => entry => entry[0] !== property

  element.properties = Object.entries(element.data)
    .filter(filter('tag'))
    .filter(filter('className'))

  element.properties = element.selfClosing
    ? element.properties
    : element.properties.filter(filter('child'))

  if (element.properties.length) {
    const attributesString = element.properties
      .map(entry => getProperty(entry))
      .join(' ')

    element.tag += ` ${attributesString}`
  }

  element.tag += '>'

  return element.tag
}

const formatProperty = (name, property) => `${name}="${property}"`

const parse = value => {
  if (isNumber(value)) {
    return value.toString()
  } else if (isFunction(value)) {
    return build(value())
  } else if (isString(value)) {
    return value
  } else if (isArray(value)) {
    return value.filter(isValid).map(build).filter(isValid).join(' ')
  } else {
    return null
  }
}

const getProperty = entry => {
  const name = entry[0]
  const value = parse(entry[1])

  if (value) {
    return formatProperty(name, value.toString())
  } else {
    return name
  }
}

const build = data => {
  const result = parse(data)

  if (result) {
    return result
  } else if (isOnlyObject(data)) {
    if (!data.tag) {
      data.tag = 'div'
    }

    const element = { data }
    element.selfClosing = isSelfClosing(data.tag)
    let output = buildTag(element)

    if (!element.selfClosing) {
      if (data.child) {
        output += build(data.child)
      }

      output += `</${data.tag}>`
    }

    return output
  }
}

const render = (data, document) => {
  const text = build(data)
  const element = document.createElement('div')
  element.innerHTML = text

  return element.childNodes
}

const editCurry = editor => (selector, data, document) => {
  const html = render(data, document)
  const element = document.querySelector(selector)

  Array.from(html).map(component => editor(component, element))
}

const append = editCurry((html, element) => element.appendChild(html))

const engine = app => {
  const viewDirectory = app.get('views')
  const basePath = path.join(process.cwd(), viewDirectory)

  return (filepath, options, callback) => {
    const state = {}
    state.dependencies = new Set()

    const depend = name => {
      state.dependencies.add(name)

      return null
    }

    state.wet = false

    const dependency = (name, element) => () => {
      if (state.wet && state.dependencies.has(name)) {
        return element
      } else {
        return null
      }
    }

    const include = (viewName, options) => {
      const jsmile = { append, build, include, depend, dependency }
      const viewPath = path.join(basePath, viewName)
      const view = require(viewPath)

      return view(jsmile, options)
    }

    const view = require(filepath)
    const jsmile = { append, build, include, depend, dependency }

    // Register dependencies
    view(jsmile, options)

    state.wet = true
    const output = view(jsmile, options)

    const rendered = build(output)

    return callback(null, rendered)
  }
}

const exports = { append, build, engine }

if (module) {
  module.exports = exports
} else {
  var jsmile = exports
  jsmile.window = window
}
