/* global firebase, firebaseui, Cookies, serverData */

const each = (selector, fn) => Array.prototype.map.call(document.querySelectorAll(selector), fn)

const authorize = token => {
  Cookies.set('__session', token)

  each('.js-authorized', element => element.classList.remove('hide'))
  each('.js-unauthorized', element => element.classList.add('hide'))
}

const unauthorize = () => {
  Cookies.remove('__session')

  each('.js-unauthorized', element => element.classList.remove('hide'))
  each('.js-authorized', element => element.classList.add('hide'))
}

const reset = () => {
  console.error('Your account is out of sync. Your page will reload.')
  document.querySelector('body').classList.add('hide')
  window.location.reload(true)
}

const uiConfig = {
  // signInSuccessUrl: '<url-to-redirect-to-on-success>',
  signInOptions: [
    // Leave the lines as is for the providers you want to offer your users.
    // firebase.auth.GoogleAuthProvider.PROVIDER_ID,
    // firebase.auth.FacebookAuthProvider.PROVIDER_ID,
    // firebase.auth.TwitterAuthProvider.PROVIDER_ID,
    // firebase.auth.GithubAuthProvider.PROVIDER_ID,
    firebase.auth.EmailAuthProvider.PROVIDER_ID
    // firebase.auth.PhoneAuthProvider.PROVIDER_ID
  ],
  callbacks: {
    signInSuccess () {
      return false
    }
  },
  // Terms of service url.
  // tosUrl: '<your-tos-url>',
  credentialHelper: firebaseui.auth.CredentialHelper.NONE
}
const ui = new firebaseui.auth.AuthUI(firebase.auth())
const initialize = () => {
  const authContainer = document.getElementById('firebaseui-auth-container')
  if (authContainer) {
    ui.start('#firebaseui-auth-container', uiConfig)
  }

  const signOutButton = document.getElementById('sign-out')
  if (signOutButton) {
    signOutButton.addEventListener('click', event => {
      firebase.auth().signOut().then(
        () => {
          Cookies.remove('__session')
          reset()
        },
        error => console.log('Error signing out:', error)
      )
    })
  }
}
initialize()

// Check if it's likely that there is a signed-in Firebase user and the session cookie expired.
// In that case we'll hide the body of the page until it will be reloaded after the cookie has been set.
// const signedInLocally = Object.keys(localStorage).find(key => key.startsWith('firebase:authUser'))
// const unidentified = signedInLocally && !Cookies.get('__session')
// if (unidentified) {
//   console.warn('You are signed in but did not receive full data from the server. Your page will reload.')
//   reset()
// }
//
// const unverified = Cookies.get('__session') && !signedInLocally
// if (unverified) {
//   console.warn('You have user credentials but are not signed in. Your page will reset.')
//   // Cookies.remove('__session')
//   reset()
// }

if (!Cookies.get('__session')) {
  document.querySelector('#firebaseui-auth-container').classList.remove('hide')
}

firebase.auth().onAuthStateChanged(function (user) {
  if (user) {
    const currentUser = firebase.auth().currentUser
    console.log('currentUser test:', currentUser)
    if (currentUser) {
      currentUser.getIdToken(true).then(token => {
        authorize(token)

        if (serverData.user) {
          console.log('You are an authorized user.')
        } else {
          console.warn('You are an authorized user but you have not received credentials. Your page will reload.')
          reset()
        }
      })
    } else {
      console.warn('You have received credentials but are not an authorized user. Your page will reload.')
      reset()
    }
  } else {
    console.log('You are not an authorized user.')
    unauthorize()
  }
})
