/* global db */

const lists = document.querySelectorAll('.list')

const queryData = {}
lists.forEach(list => {
  const dataset = {
    collection: list.dataset.collection,
    where: list.dataset.where
  }
  const dataString = JSON.stringify(dataset)

  if (queryData[dataString]) {
    queryData[dataString].push(list)
  } else {
    queryData[dataString] = [list]
  }
})

Object.entries(queryData).map(entry => {
  const queryDatum = JSON.parse(entry[0])
  const collectionRef = db
    .collection(queryDatum.collection)

  const lists = entry[1]

  const query = queryDatum.where
    ? collectionRef.where(...queryDatum.where)
    : collectionRef
  query
    .onSnapshot(items => {
      lists
        .forEach(list => {
          const links = list
            .querySelectorAll('.link')

          // Get ids of elements currently on the page.
          const linkIds = []
          links.forEach(link => {
            const linkId = link.dataset.linkid

            linkIds.indexOf(linkId) > -1
              ? linkIds.push(linkId)
              : link.remove()
          })

          // Add each id from database that is missing
          items.forEach(item => {
            const itemId = queryDatum.collection === 'users'
              ? item.uid
              : item.id

            if (linkIds.indexOf(itemId) === -1) {
              const id = document.createElement('span')
              id.classList.add('id')
              id.appendChild(
                document.createTextNode(itemId)
              )

              const anchor = document.createElement('a')
              anchor.appendChild(id)
              anchor.classList.add('link')
              anchor.setAttribute(
                'data-linkid', itemId
              )
              anchor.setAttribute(
                'href', `/${queryDatum.collection}/${itemId}`
              )

              list.appendChild(anchor)
            }
          })
        })
    })
})
