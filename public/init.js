/* global firebase */

console.log('hostname test:', window.location.origin)

// Initialize Firebase
var config = {
  apiKey: 'AIzaSyCMqO7Mqh8J9sCHWqWjsmC2oGYHxwmkcew',
  authDomain: 'killers-812ba.firebaseapp.com',
  databaseURL: 'https://killers-812ba.firebaseio.com',
  projectId: 'killers-812ba',
  storageBucket: 'killers-812ba.appspot.com',
  messagingSenderId: '882337942394'
}
firebase.initializeApp(config)

var db = firebase.firestore()
db.settings({ timestampsInSnapshots: true })

var onConnect = firebase.functions().httpsCallable('onConnect')
console.log('onConnect test:', onConnect)
const connection = onConnect({ text: 'hello world!' })
console.log('connection test:', connection)
connection.then(function (result) {
  console.log('connection result test:', result)
}).catch(error => {
  throw error
})

// var api = (name, data) => {
//   const fn = firebase.functions().httpsCallable(name)
//   if (data) {
//     return fn(data)
//   } else {
//     return fn()
//   }
// }
// api('onConnect').then(result => {
//   console.log('result test:', result)
// }).catch(error => {
//   console.log('error.code test:', error.code)
//   console.log('error.message test:', error.message)
//   console.log('error.details test:', error.details)
// })
//
