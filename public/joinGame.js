/* global firebase */

document.querySelectorAll('.join-game')
  .forEach(element => element
    .addEventListener('click', event => {
      console.log(
        'gameid test:', element.dataset.gameid
      )

      element.innerHTML = 'Loading...'

      const joinGame = firebase
        .functions()
        .httpsCallable('joinGame')

      console.log('joinGame test:', joinGame)

      joinGame({ gameId: element.dataset.gameid })
        .then(result => {
          console.log('joinGame result test:', result)
          element.innerHTML = result.data.playerId
        })
        .catch(error => {
          console.error(error)
        })
    })
  )
