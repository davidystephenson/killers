/* global firebase */

const buttons = document
  .querySelectorAll('.new-game')

buttons
  .forEach(button => button.addEventListener(
    'click',
    function handler (event) {
      event.preventDefault()

      button.innerHTML = 'Loading...'

      const newGame = firebase
        .functions()
        .httpsCallable('newGame')

      newGame().then(result => {
        const link = document.createElement('a')
        link.setAttribute(
          'href', `/game/${result.data.gameId}`
        )

        const id = document.createElement('span')
        id.classList.add('id')
        id.innerHTML = result.data.gameId
        link.appendChild(id)

        button.classList.remove('new-game')
        button.removeEventListener('click', handler)
        button.innerHTML = ''
        button.appendChild(link)
      })
    }
  ))
